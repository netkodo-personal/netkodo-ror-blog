# Repository for Rails api with Vue frontend

* `blog-backend/`  - `Rails ~ 6.1.4` api server, backend
* `blog-frontend/` - `Vuejs ~ 3` application frontend
* `rails-api-vue/` - Folder with docker, docker-compose configuration

### How to run servers(inside docker):
#### Backend(Rails API)
- enter folder `blog-backend/`
- run `rails s -b 0.0.0.0`
#### Frontend(Vuejs)
- enter folder `blog-frontend/`
- run [1] `pnpm serve -- --host 0.0.0.0`

##### notice [1]: `pnpm` is package manager 3rd party replacement for `npm`, `yarn`, ... Use analogous commend to your package manager. 

### Tasks:
- [x] Post CRUD implementation in frontend
	- [x] Create
		- [x] DONE required handling of 'C'
		- [x] client side validation
		- [x] client side server validation handling
	- [x] Read 
	- [x] Update 
		- [x] client side validation
		- [x] client side server validation handling
	- [x] Delete 
- [x] Comment CRUD implementation in frontend
	- [x] Create
		- [x] client side validation
		- [x] client side server validation handling
	- [x] Read 
	- [x] Update 
		- [x] DONE fix editing mode, enter edit mode for all comments
		- [x] client side validation
		- [x] client side server validation handling
	- [x] Delete 
- [ ] User CRUD implementation in frontend
	- [x] Create 
		- [x] client side validation
		- [x] client side server validation handling
	- [x] Read
	- [ ] Update 
		- [ ] client side validation
		- [ ] client side server validation handling
	- [ ] Delete
- [x] Authentication for
	- [x] User Login
	- [x] Post
		- [x] CRUD
	- [x] Comment
		- [x] CRUD
- [x] Authorization
	- [x] client-side
		- [x] Posts
		- [x] Comments
	- [x] server-side
		- [x] Posts
		- [x] Comments
- [x] JBuilder 
	- [x] comments
		- [x] index
		- [x] show 
	- [x] posts
		- [x] index


### Technologies/Resources used:
- [Ruby on Rails 6](https://rubyonrails.org/) as API, backend server
	- [devise](https://github.com/heartcombo/devise) for authentication
	- [devise-jwt](https://github.com/waiting-for-dev/devise-jwt) for token authentication
	- [jbuilder](https://github.com/rails/jbuilder)
- [Vuejs 3](https://v3.vuejs.org/) as frontend
	- [Vue Router 4](https://next.router.vuejs.org/) for routing on Single Page
	- [Vee-Validate 4](https://vee-validate.logaretm.com/v4/) Form Validation for Vue.js
	- [Vuex 4](https://next.vuex.vuejs.org/) for global state managment
- [Skeleton](http://getskeleton.com/) for some bare css
- [Docker](https://www.docker.com/) for separation and easy management of project environment
	- [Docker-compose](https://docs.docker.com/compose/) for managment of dockers 