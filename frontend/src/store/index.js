import { createStore } from 'vuex';
import axios from 'axios';
import router from '@/router'

const store = createStore({
  modules: {
    authentication: {
      namespaced: true,
      
      state: {
        token: '',
        logged_in: false,
        is_admin: false,
        user_email: ''
      },
      
      getters: {
        get_token(state){ 
          return state.token; 
        },
        get_logged_state(state) {
          return state.logged_in; 
        },        
        get_is_admin(state) {
          return state.is_admin; 
        },
        get_user_email(state) {
          return state.user_email;
        },
      },

      mutations: {
        set_token(state, token) {
          state.token = token;
        },
        set_logged_state(state, value) {
          state.logged_in = value;
        },
        set_is_admin(state, is) {
          state.is_admin = is;
        },
        set_user_email(state, email) {
          state.user_email = email;
        },
      },
      actions: {
        check_exist({commit}) {
          if(localStorage.getItem("token")){
            commit('set_token', localStorage.getItem("token")); 
            commit('set_logged_state', true); 
            
            if(localStorage.getItem("is_admin") === "true") {
              commit('set_is_admin', true);
            } else if(localStorage.getItem("is_admin") === "false") {
              commit('set_is_admin', false);
            }

            commit('set_user_email', localStorage.getItem("user_email"));
            return true;
          }
          commit('set_token', ""); 
          commit('set_logged_state', false); 
          commit('set_is_admin', false); 
          commit('set_user_email', "");
          return false;
        },

        

        async post_axios_comment({state}, payload) {
          const url = `api/v1/posts/${payload.post_id}/comments`;

          const header = {
            'Authorization': state.token
          };

          return axios.post(url, payload, { headers: header })          
        },

        async patch_axios_comment({state}, payload) {
          const payload_2 = {
            post_id: payload.post_id,
            author: payload.author,
            body: payload.body,
          }
          const url = `api/v1/posts/${payload.post_id}/comments/${payload.id}`;
          const header = {
            'Authorization': state.token
          };

          return axios.patch(url, payload_2, { headers: header })          
        },

        async get_axios_comments({}, post_id) {
          const url = `api/v1/posts/${post_id}/comments`;

          return axios.get(url);
        },

        async delete_axios_comment({state}, ids) {
          const url = `api/v1/posts/${ids.post_id}/comments/${ids.comm_id}`;
          const header = {
            'Authorization': state.token
          };

          return axios.delete(url, {headers: header});
        },
        
        async post_axios_post({state}, post) {
          const url = 'api/v1/admin/posts';
          const payload = {
            title: post.title,
            body: post.body,
          };
          const header = {
            'Authorization': state.token
          };
          return axios.post(url, payload, { headers: header });
        },
        async patch_axios_post({state}, post) {
          const url = `api/v1/admin/posts/${post.id}`;
          const payload = {
            title: post.title,
            body: post.body,
          };
          const header = {
            'Authorization': state.token
          };
          return axios.patch(url, payload, { headers: header });
        },
        async delete_axios_post({state}, post) {
          const url = `api/v1/admin/posts/${post.id}`;
          const header = {
            'Authorization': state.token
          }

          return axios.delete(url, { headers: header });
        },

        async get_axios_posts({}, payload) {
          const url = 'api/v1/posts';
          return axios.get(url, {params: payload})
        },

        async admin_get_axios_posts({state}) {
          const url = 'api/v1/admin/posts';
          const header = {
            'Authorization': state.token
          };
          return axios.get(url, { headers: header })
        },

        async get_axios_post({state}, post_id) {
          const url = `api/v1/posts/${post_id}`;
          const header = {
            'Authorization': state.token
          };
          
          return axios.get(url, { headers: header });
        },

        async login({commit}, {email, password}) {
          const path = 'users/sign_in/'
          const payload = {
            'user': {
              'email': email, 
              'password': password
            }};
          
          axios.post(path, payload)
            .then((resp) => {
              console.log(resp)
              commit('set_token', resp.headers.authorization);
              commit('set_logged_state', true);
              commit('set_is_admin', resp.data.is_admin);
              commit('set_user_email', payload.user.email);

              localStorage.setItem("token",  resp.headers.authorization);
              localStorage.setItem("is_admin",  resp.data.is_admin);
              localStorage.setItem("user_email", payload.user.email);

              router.push('/');
            })
            .catch((err) => console.log(err));
        },
        async logout({commit, state}) {
          const url = 'users/sign_out/'
          // tu jest wymagany token
          const header = {
            'Authorization': state.token
          }
          axios.delete(url, {headers: header})
            .then((resp) => {
              console.log(resp);
              localStorage.removeItem('token');
              localStorage.removeItem('is_admin');
              localStorage.removeItem('user_email');

              commit('set_token', ""); 
              commit('set_logged_state', false); 
              commit('set_is_admin', false); 
              commit('set_user_email', "");
            })
            .catch((err) => console.log(err));
        },

        async register({commit}, {email, password}) {
          const path = 'users/'
          const payload = { 
            'user': { 
              'email': email, 
              'password': password 
            }};

          axios.post(path, payload)
            .then((resp) => {
              console.log(resp)
            })
            .catch((err) => console.log(err));
        },
      },
    }
  },
});

export default store;