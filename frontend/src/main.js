import { createApp } from 'vue';

import App from './App.vue';
import store from './store';
import router from './router';
import axios from 'axios';

// defautl settings for axios 
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.baseURL = 'http://localhost:3000/'

const app = createApp(App)
              .use(router)
              .use(store)
              .mount('#app');
