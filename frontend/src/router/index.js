import { createRouter, createWebHistory } from 'vue-router';
import Posts from '@/views/content/Posts.vue';
import store from '@/store/index.js';

const routes = [
  {
    path: '/',
    component: Posts,
  },
  {
    path: '/post/:id',
    component: () => import('@/views/content/Post.vue'),
  },
  {
    path: '/login',
    component: () => import('@/views/auth/Login.vue'),
  },
  {
    path: '/register',
    component: () => import('@/views/auth/Register.vue'),
  },
  {
    path: '/about',
    component: () => import('@/views/content/About.vue'),
  },
  {
    path: '/admin',
    component: () => import('@/views/admin/Admin.vue'), 
  },
  { // default not found resource page
    path: '/:pathMatch(.*)*', 
    name: 'not-found',
    component: () => import('@/views/NotFound'), 
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from) => {
  if(to.path === '/admin') {
    store.dispatch['authentication/check_exist'];
    if(store.getters['authentication/get_is_admin']){
      return true;
    } else {
      router.push('not-found')
      return false;
    }
  };
  return true;
});

export default router;
