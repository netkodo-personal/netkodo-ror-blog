Rails.application.routes.draw do
  root to: 'posts#index'

  devise_for :users,
    controllers: {
      sessions: 'users/sessions',
      registrations: 'users/registrations'
    },
    defaults: { format: :json }

  namespace :api, 
    constraints: {format: :json}, 
    defaults: {format: :json} do
    namespace :v1 do

      resources :posts, only: [:show, :index] do
        resources :comments, only: [:show, :index, :update, :edit, :new, :create]
      end

      namespace :admin do
        resources :posts, only: [:edit, :update, :new, :create, :destroy] do
          resources :comments
        end
      end

    end
  end

end
