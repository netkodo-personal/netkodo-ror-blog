class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, 
         :jwt_authenticatable,
         :registerable, 
         :validatable,
         password_length: 8..70,
         jwt_revocation_strategy: JwtDenylist 


  # validation of user email
  validates :email, presence: true, uniqueness: true
  validates :email, length: { minimum: 3, too_short: "%{count} character is the minimum allowed." }
  validates :email, length: { maximum: 50, too_long: "%{count} character is the maximum allowed." }

  
  # reaped from: https://github.com/heartcombo/devise/wiki/How-To:-Set-up-simple-password-complexity-requirements
  # and modified for password confirmation
  validate :password_complex_n_complexity_validation
  def password_complex_n_complexity_validation
    # Regexp extracted from https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a
    return if password.blank? || password_confirmation.blank? || password == password_confirmation || password =~ /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,70}$/ || password_confirmation =~ /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,70}$/
    # above monstrosity checks if
    # => password is not blank
    # => password_confirmation is not blank
    # => password and password_confirmation are the same
    # => password is checked withe regex(link above) checking for password length and all characters, comment below.

    errors.add :password, 'Complexity requirement not met. Length should be 8-70 characters and include: 1 uppercase, 1 lowercase, 1 digit and 1 special character'
  end
end