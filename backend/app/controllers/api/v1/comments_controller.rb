module Api
  module V1
    class CommentsController < ApplicationController
      before_action :set_comment, only: [:show, :update, :destroy]
      # skip_before_action :authenticate_user!, only: %i[index, show]
      before_action :authenticate_user!, except: [:index, :show]

      # GET /comments
      def index
        respond_to do |format|
          @comments = Comment.where(post_id: params[:post_id])
          format.json
        end
        # render json: @comments
      end

      # GET /comments/1
      def show
        respond_to do |format|
          format.json
        end
      end

      # POST /comments
      def create
        authorize Comment, :create?

        @comment = Comment.new(comment_params)

        if @comment.save
          render json: @comment, status: :created
        else
          render json: @comment.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /comments/1
      def update
        authorize @comment, :update?

        if @comment.update(comment_params)
          render json: @comment
        else
          render json: @comment.errors, status: :unprocessable_entity
        end
      end

      # DELETE /comments/1
      def destroy
        authorize @comment, :destroy?

        @comment.destroy
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_comment
          @comment = Comment.find(params[:id])
        end

        # Only allow a list of trusted parameters through.
        def comment_params
          params.require(:comment).permit(:author, :body, :post_id)
        end
    end
  end
end
