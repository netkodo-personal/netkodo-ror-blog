class Api::V1::Admin::PostsController < ApplicationController
  before_action :set_post, only: [:update, :destroy]
  before_action :authenticate_user!
  before_action :require_admin

	def create
		authorize Post, :create?

		pp = post_params
		if Post.count == 0
			pp[:id] = 1
		else
			pp[:id] = Post.last.id + 1
		end
		@post = Post.new(pp)

		if @post.save
			render json: @post, status: :created
		else
			render json: @post.errors, status: :unprocessable_entity
		end
	end

	def update
		authorize @post, :update?

		if @post.update(post_params)
			render json: @post
		else
			render json: @post.errors, status: :unprocessable_entity
		end
	end

	def destroy
		authorize @post, :destroy?

		@post.destroy
	end

	private
	def set_post
		@post = Post.find(params[:id])
	end
	def post_params
		params.require(:post).permit(:title, :body)
	end
end