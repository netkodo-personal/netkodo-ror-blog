module Api
  module V1
    class PostsController < ApplicationController
      before_action :set_post, only: [:show, :update, :destroy]
      # skip_before_action :authenticate_user!, only: %i[index, show]
      before_action :authenticate_user!, except: [:index, :show]

      # GET /posts
      def index
        respond_to do |format|
          @posts = Post.all
          @full = params[:full]
          format.json
        end
      end

      # GET /posts/1
      def show
        # respond_to do |format|
        #   format.json 
        # end
        render json: @post
      end

      # POST /posts
      def create
        authorize Post, :create?

        pp = post_params
        if Post.count == 0
          pp[:id] = 1
        else
          pp[:id] = Post.last.id + 1
        end
        @post = Post.new(pp)

        if @post.save
          render json: @post, status: :created
        else
          render json: @post.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /posts/1
      def update
        authorize @post, :update?

        if @post.update(post_params)
          render json: @post
        else
          render json: @post.errors, status: :unprocessable_entity
        end
      end

      # DELETE /posts/1
      def destroy
        authorize @post, :destroy?

        @post.destroy
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_post
          @post = Post.find(params[:id])
        end

        # Only allow a list of trusted parameters through.
        def post_params
          params.require(:post).permit(:title, :body)
        end
    end
  end
end