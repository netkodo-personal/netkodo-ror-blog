class ApplicationController < ActionController::API
  include Pundit
  include ActionController::MimeResponds

  before_action :authenticate_user!

  def require_admin
    unless current_user.admin?
      redirect_to root_path
    end
  end
end
