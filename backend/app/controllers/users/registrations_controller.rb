class Users::RegistrationsController < Devise::RegistrationsController

  respond_to :json

  private

  def respond_with(resource, _opts = {})
    if resource.persisted?
      return register_success 
    end

    register_failed
  end

  def register_success
    render json: { message: 'Signed up successfully.' }, status: 201
  end

  def register_failed
    render json: { message: 'Something went wrong.' }, status: 400
  end
end
