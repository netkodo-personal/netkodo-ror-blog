# frozen_string_literal: true

class CommentPolicy < ApplicationPolicy
  attr_accessor :user, :comment

  def initialize(user, comment)
    @user = user
    @comment = comment
  end

  def create?
    is? || admin?
  end

  def new?
    create?
  end

  def update?
    is? && (author? || admin?)
  end

  def edit?
    update?
  end

  def destroy?
    is? && (author? || admin?)
  end

  private

  # @return [TrueClass, FalseClass]
  def is?
    !user.nil?
  end

  # @return [TrueClass, FalseClass]
  def author?
    user.email == comment.author
  end

  # @return [TrueClass, FalseClass]
  def admin?
    user.admin?
  end

end
