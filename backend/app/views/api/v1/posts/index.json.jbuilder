if @full
	json.array! @posts do |post|
		json.post post
		json.comments Comment.where(post_id: post.id)
	end
else
	json.array! @posts, :id, :title, :created_at
end
